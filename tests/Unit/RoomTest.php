<?php

namespace Tests\Unit;

use App\Floor;
use App\Room;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoomTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /** @test */
    public function a_room_belongs_to_a_floor()
    {
        $room = factory(Room::class, 1)->create()->first();
        $floor = factory(Floor::class, 1)->create(["floor" => 1])->first();

        $floor->addRoom($room);

        $this->assertEquals($floor->id, $room->floor_id);
    }
}
