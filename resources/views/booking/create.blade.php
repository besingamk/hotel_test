@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <form method="post" action="{{ route('books.store', ['id' => $room->id]) }}">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" name="customer_name" class="form-control  @error('customer_name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name"  value="{{ old('customer_name') }}">
                                    @error('customer_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your name with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="customer_email" class="form-control @error('customer_email') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"  value="{{ old('customer_email') }}">
                                    @error('customer_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <input type="date" name="customer_arrival" class="form-control @error('customer_arrival') is-invalid @enderror" min="{{ \Carbon\Carbon::now()->toDateString() }}"  value="{{ old('customer_arrival') }}">
                                    @error('customer_arrival')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your arrival with anyone else.</small>
                                </div>
                                <input type="hidden" name="room_id" value="{{ $room->id }}">
                                <button type="submit" class="btn btn-primary">Continue <i class="fa fa-chevron-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
