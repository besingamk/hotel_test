@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Rooms</h1>
                <div class="row">
                    @foreach ($rooms as $room)
                        <div class="col-md-4 mt-4">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Room #: {{ $room->room_number }}</h5>
                                </div>

                                <div class="card-body">
                                    <div class="alert alert-{{ $room->availability ? 'success' : 'danger' }}" role="alert">
                                        <i class="fas fa-{{ $room->availability ? 'check' : 'times' }}"></i> Available
                                    </div>
                                    <div class="alert alert-{{ $room->floor()->first()->gulf_view ? 'success' : 'danger' }}" role="alert">
                                        <i class="fas fa-{{ $room->floor()->first()->gulf_view ? 'check' : 'times' }}"></i> Gulf View
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <a href="{{ route('rooms.show', ['room' => $room->id ]) }}" class="btn btn-primary btn-small">View</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
