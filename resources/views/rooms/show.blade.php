@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Room #: {{ $room->room_number }}</h1>
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <img src="https://picsum.photos/id/1060/600/200" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Floor #: {{ $room->floor()->first()->floor }}, Room #: {{ $room->room_number }}</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed aliquet malesuada elementum. Suspendisse fermentum enim neque, nec malesuada est lobortis bibendum. Proin sed sem bibendum, congue orci sit amet, laoreet urna. Vivamus vitae turpis molestie, cursus velit vel, laoreet mi. Vestibulum eget tortor eu neque varius lobortis. In tristique neque ac bibendum sodales. Duis feugiat metus a magna pharetra sagittis. Fusce venenatis lorem eu erat molestie faucibus. Fusce consequat metus a velit hendrerit, vel malesuada velit tempor. Nullam nibh risus, lacinia vitae tempor ac, placerat vel enim. Vestibulum lobortis vehicula lectus sed consectetur. Morbi malesuada odio ullamcorper turpis ultricies feugiat. Nulla facilisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent quis commodo nisi.</p>
                            <h6>Gulf View: {{ $room->floor()->first()->gulf_view ? 'Yes' : 'No' }}</h6>
                            <a href="{{ route('books.create', ['id' => $room->id]) }}" class="btn btn-primary {{ $room->availability ? '' : 'disabled' }}">{{ $room->availability ? 'Book now!' : 'Sorry this room is not available.' }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
