<?php

use Illuminate\Database\Seeder;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Floor::class, 5)->create()
            ->each(function ($floor) {
                $floor->floor = $floor->id;
                $floor->gulf_view = rand(0, 1);
                $floor->save();
                factory(App\Room::class, 20)->create()
                    ->each(function ($room) use ($floor) {
                        $room->availability = rand(0, 1);
                        $floor->addRoom($room);
                    });
            });
    }
}
