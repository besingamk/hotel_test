<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Admin",
            'email' => "admin@hotel.com",
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => bcrypt("password"),
            'role' => 'admin'
        ]);

        User::create([
            'name' => "Receptionist",
            'email' => "receptionist@hotel.com",
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => bcrypt("password"),
            'role' => 'receptionist'
        ]);
    }
}
