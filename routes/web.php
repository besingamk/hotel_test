<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('rooms', 'RoomsController');

Route::resource('room/{id}/books', 'BookingsController')->except([
    'index', 'show', 'edit', 'update', 'destroy'
]);;

Route::resource('analytics', 'AnalyticsController')->middleware('auth');
Route::resource('customers/{q?}', 'CustomersController', ['as' => 'customers'])->middleware('auth');