<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function addCustomer(Customer $customer)
    {
        $this->customer_id = $customer->id;
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    public function addRoom(Room $room)
    {
        $this->room_id = $room->id;
    }

    public  function room()
    {
        return $this->hasOne(Room::class, 'id','room_id');
    }
}
