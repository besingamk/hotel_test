<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    public function addRoom(Room $room)
    {
        $this->rooms()->save($room);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
