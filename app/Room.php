<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function toNotAvailable()
    {
        $this->availability = false;
    }
}
