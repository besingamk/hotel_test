<?php

namespace App\Observers;

use App\Booking;
use App\Notifications\CustomerBookingNotification;
use Illuminate\Support\Facades\Log;

class BookingObserver
{
    /**
     * Handle the booking "created" event.
     *
     * @param  \App\Booking  $booking
     * @return void
     */
    public function created(Booking $booking)
    {
        Log::info("A new booking has created with an booking ID of " . $booking->id);
        $room = $booking->room()->first();
        $room->toNotAvailable();
        $room->save();
        $booking->customer()->first()->notify(new CustomerBookingNotification());
    }

    /**
     * Handle the booking "updated" event.
     *
     * @param  \App\Booking  $booking
     * @return void
     */
    public function updated(Booking $booking)
    {
        //
    }

    /**
     * Handle the booking "deleted" event.
     *
     * @param  \App\Booking  $booking
     * @return void
     */
    public function deleted(Booking $booking)
    {
        //
    }

    /**
     * Handle the booking "restored" event.
     *
     * @param  \App\Booking  $booking
     * @return void
     */
    public function restored(Booking $booking)
    {
        //
    }

    /**
     * Handle the booking "force deleted" event.
     *
     * @param  \App\Booking  $booking
     * @return void
     */
    public function forceDeleted(Booking $booking)
    {
        //
    }
}
