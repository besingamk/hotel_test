# INSTALLATION
### SERVER REQUIREMENTS
1. php 7.1.3
2. mysql

### GETTING THE CODE
clone the source code from bitbucket 
``` bash
git clone https://besingamk@bitbucket.org/besingamk/hotel_test.git
```
and move inside the project directory
``` bash
cd hotel_test
```

### CONFIGURATION
copy the `.env.example` to `.env`
``` bash
cp .env.example .env
```

and changes the configuration inside `.env` base on your dabase credentials

### DEPENDENCIES
run the composer
``` bash
composer install
```
run the database migrations, seeders and factory
``` bash
php artisan migrate
php artisan db:seed --class=UsersSeeder
php artisan db:seed --class=RoomsSeeder
```

### RUNNING THE APPLICATION
``` bash
php artisan serve
```
<hr />

## TESTING
copy the `.env.example` to `.env.testing`
``` bash
cp .env.example .env.testing
```
and changes the configuration inside `.env` base on your testing dabase credentials

and run
``` bash
vendor/bin/phpunit --testdox
 ```